import Foundation
import UIKit
import VisioMoveEssential

@objc(VisioGlobeMap)
public class VisioGlobeMap : NSObject {
    var mapView: VMEMapView;
    
    @objc
    public init(frame: CGRect) {
        mapView = VMEMapView(frame: frame);
    }
    
    @objc
    public func setMapBundle(mapPath: String, mapSecretCode: Int32) -> Self {
        mapView.mapPath = mapPath;
        mapView.mapSecretCode = mapSecretCode;
        
        return self;
    }
    
    @objc
    public func setMapHash(mapHash: String) -> Self {
        mapView.mapHash = mapHash;
        
        return self;
    }
    
    @objc
    public func getMapView() -> UIView {
        return mapView;
    }
    
    @objc
    public func loadMap() -> Void {
        mapView.loadMap();
    }
}
