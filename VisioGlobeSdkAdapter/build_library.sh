#!/usr/bin/env zsh

xcodebuild -sdk iphoneos -arch arm64 -configuration Release 

xcodebuild -sdk iphonesimulator -arch x86_64 -configuration Release

cd build

cp -R "Release-iphoneos" "Release-fat"

cp -R "Release-iphonesimulator/VisioGlobeSdkAdapter.framework/Modules/VisioGlobeSdkAdapter.swiftmodule/" "Release-fat/VisioGlobeSdkAdapter.framework/Modules/VisioGlobeSdkAdapter.swiftmodule/"

lipo -create -output "Release-fat/VisioGlobeSdkAdapter.framework/VisioGlobeSdkAdapter" "Release-iphoneos/VisioGlobeSdkAdapter.framework/VisioGlobeSdkAdapter" "Release-iphonesimulator/VisioGlobeSdkAdapter.framework/VisioGlobeSdkAdapter"

sharpie bind --sdk=iphoneos15.2 --output="XamarinApiDef" --namespace=“VisioGlobeSdkAdapter” --scope="Release-fat/VisioGlobeSdkAdapter.framework/Headers/" "Release-fat/VisioGlobeSdkAdapter.framework/Headers/VisioGlobeSdkAdapter-Swift.h"
