//
//  VMELog+CoreDataProperties.h
//  
//
//  Created by Fanny Vignal on 02/11/2021.
//
//  This file was automatically generated and should not be edited.
//

#import "VMELog+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VMELog (CoreDataProperties)

+ (NSFetchRequest<VMELog *> *)fetchRequest NS_SWIFT_NAME(fetchRequest());

@property (nullable, nonatomic, copy) NSString *data;

@end

NS_ASSUME_NONNULL_END
