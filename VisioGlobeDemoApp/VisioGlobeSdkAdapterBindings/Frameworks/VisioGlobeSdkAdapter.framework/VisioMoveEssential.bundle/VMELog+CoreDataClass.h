//
//  VMELog+CoreDataClass.h
//  
//
//  Created by Fanny Vignal on 02/11/2021.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface VMELog : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "VMELog+CoreDataProperties.h"
