﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace VisioGlobeSdkAdapter
{
	// @interface VisioGlobeMap : NSObject
	[BaseType(typeof(NSObject))]
[DisableDefaultCtor]
interface VisioGlobeMap
{
	// -(instancetype _Nonnull)initWithFrame:(CGRect)frame __attribute__((objc_designated_initializer));
	[Export("initWithFrame:")]
	[DesignatedInitializer]
	IntPtr Constructor(CGRect frame);

	// -(instancetype _Nonnull)setMapBundleWithMapPath:(NSString * _Nonnull)mapPath mapSecretCode:(int32_t)mapSecretCode __attribute__((warn_unused_result("")));
	[Export("setMapBundleWithMapPath:mapSecretCode:")]
	VisioGlobeMap SetMapBundleWithMapPath(string mapPath, int mapSecretCode);

	// -(instancetype _Nonnull)setMapHashWithMapHash:(NSString * _Nonnull)mapHash __attribute__((warn_unused_result("")));
	[Export("setMapHashWithMapHash:")]
	VisioGlobeMap SetMapHashWithMapHash(string mapHash);

	// -(UIView * _Nonnull)getMapView __attribute__((warn_unused_result("")));
	[Export("getMapView")]
	UIView MapView { get; }

	// -(void)loadMap;
	[Export("loadMap")]
	void LoadMap();
}
}
