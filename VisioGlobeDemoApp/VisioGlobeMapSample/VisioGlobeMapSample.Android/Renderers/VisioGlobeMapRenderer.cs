﻿using Android.Content;
using VisioGlobeMapSample;
using VisioGlobeMapSample.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Com.Visioglobe.Visiomoveessential;
using Com.Visioglobe.Visiomoveessential.Listeners;
using Com.Visioglobe.Visiomoveessential.Models;
using RelativeLayout = Android.Widget.RelativeLayout;

[assembly: ExportRenderer(typeof(VisioGlobeMap), typeof(VisioGlobeMapRenderer))]
namespace VisioGlobeMapSample.Droid.Renderers
{
    public class VisioGlobeMapRenderer : ViewRenderer<VisioGlobeMap, Android.Views.View>
    {
        public VisioGlobeMapRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VisioGlobeMap> e)
        {
            base.OnElementChanged(e);
            
            var parent = new FrameLayout(Context);

            var mapView = new VMEMapView(parent.Context, null);
            mapView.PromptUserToDownloadMap = false;

            //parent.LayoutParameters = new RelativeLayout.LayoutParams(
            //    LayoutParams.MatchParent,
            //    LayoutParams.MatchParent
            //);

            //mapView.LayoutParameters = new RelativeLayout.LayoutParams(
            //    LayoutParams.MatchParent,
            //    LayoutParams.MatchParent
            //);

            mapView.SetMapSecretCode(0);
            mapView.MapHash = "ma10a386909f58fb12f8bbde9c3111c46077f485d";
            mapView.MapPath = "asset://map_bundle.zip";
            mapView.SetLifeCycleListener(new LifeCycleListener(Context));
            parent.AddView(mapView);

            SetNativeControl(parent);
            mapView.LoadMap();
        }
    }

    class LifeCycleListener : VMELifeCycleListener
    {
        private readonly Context? _context;

        public LifeCycleListener(Context? context)
        {
            _context = context;
        }

        public override void MapDidInitializeEngine(VMEMapView mapView)
        {
            //String lFilePath = extractFromAssetsAndGetFilePath("artifika_regular.ttf");
            //if (lFilePath != null)
            //{
            //    mapView.setMapFont(lFilePath);
            //}
        }

        public override void MapDidDisplayRoute(VMEMapView p0, VMERouteResult p1)
        {
            base.MapDidDisplayRoute(p0, p1);

            Toast.MakeText(_context, "Route displayed", ToastLength.Long).Show();
        }

        public override void MapDidLoad(VMEMapView p0)
        {
            Toast.MakeText(_context, "Map loaded", ToastLength.Long).Show();
        }

        public override void MapDidDisplayPlaceInfo(VMEMapView p0, string p1)
        {
            Toast.MakeText(_context, p1, ToastLength.Long).Show();
        }
    }
}