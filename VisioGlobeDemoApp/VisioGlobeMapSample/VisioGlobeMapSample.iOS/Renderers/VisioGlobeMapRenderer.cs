﻿using System;
using CoreGraphics;
using UIKit;
using VisioGlobeSdkAdapter;
using VisioGlobeMapSample;
using VisioGlobeMapSample.iOS;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.IO;
using Foundation;

[assembly: ExportRenderer(typeof(VisioGlobeMapSample.VisioGlobeMap), typeof(VisioGlobeMapRenderer))]
namespace VisioGlobeMapSample.iOS
{
    public class VisioGlobeMapRenderer : ViewRenderer<VisioGlobeMap, UIView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<VisioGlobeMap> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe from event handlers and cleanup any resources
            }

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    // Instantiate the native control and assign it to the Control property with
                    // the SetNativeControl method

                    //var control = ConvertFormsToNative(new Label(), new CGRect(0, 0, 300, 256));

                    //var size = base.SizeThatFits();
                    //this.fr

                    //var size = base.SizeThatFits(new CGSize(3000, 3000));

                    //var parent = new UIView(new CGRect(0, 0, 300, 256));

                    var map = new VisioGlobeSdkAdapter.VisioGlobeMap(Frame);

                    var bundlePath = Path.Combine(NSBundle.MainBundle.BundlePath, string.Format("MapBundle"));

                    map.SetMapBundleWithMapPath("MapBundle", 0); 
                    //map.SetMapHashWithMapHash("mc8f3fec89d2b7283d15cfcf4eb28a0517428f054");

                    var mapView = map.MapView;


                    map.LoadMap();

                    SetNativeControl(mapView);
                }
                // Configure the control and subscribe to event handlers
            }

            //SetNeedsDisplay();
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            return;
            if (e.PropertyName == VisualElement.WidthProperty.PropertyName ||
                e.PropertyName == VisualElement.HeightProperty.PropertyName)
            {
                if (Element != null && Element.Bounds.Height > 0 && Element.Bounds.Width > 0)
                {
                    SetNeedsDisplay();
                    //update size on native control's subviews
                    // or use SetNeedsDisplay() to re-draw
                    //Note: force update to Control's size is not recommended (as it is recommended it be same as forms element)
                    //Instead add a subview that you can update for size here
                }
            }
        }
    }
}

